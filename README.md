# Start

Build a new website using NPM as a build tool.

## Initial Setup

- Clone repo

    **SSH**

        git clone git@bitbucket.org:courtneymyers/start.git

    **HTTPS**

        git clone https://courtneymyers@bitbucket.org/courtneymyers/start.git

---

- Change directory to cloned repo

        cd start

---

- Install node modules

        npm install

---

- Setup public directory

        npm run setup

---

- Start Build Process

        npm start

---

- Modify code and watch it update live in the browser :)

## Build Process

After initial setup, to kick off the build process, simply run:

    npm start